# Generated by Django 4.2.1 on 2023-05-22 18:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('capsule', '0003_alter_launche_details'),
    ]

    operations = [
        migrations.AlterField(
            model_name='launche',
            name='static_fire_date_utc',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
