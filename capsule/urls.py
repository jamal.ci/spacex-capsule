from django.urls import path
from .views import *

app_name = 'capsule'
urlpatterns = [
    #path('', index, name="welcome"),
    path('api', get_capsule_api, name="capsule_api"),
    path('', capsule_list, name="capsule_list"),
    path('detail/<str:id>', capsule_detail, name="capsule_detail"),
]
