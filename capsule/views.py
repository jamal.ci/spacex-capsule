from django.shortcuts import redirect, render
from .models import *
from services.spacex_service import SpaceXService
from utils.custom_functions import find, find_all, save_instance
from django.contrib import messages
#import spacexpy


# ---------------- Get capsules by API  -----------------
def get_capsule_api(request):

    #spacex = spacexpy.SpaceX()
    #cl = spacex.capsules()
    #print(cl)
    # Spacex Api params
    end_point = 'capsules'
    params = {}
    spacex = SpaceXService()
    # Get list of capsules
    capsules = spacex.get_full_data_request(end_point, params)

    responseJson = capsules.json()
    if responseJson:
        for caps in responseJson:

            # Check if capsules exist before save in Sqlite
            capsule = find(Capsule, {'capsule_id': caps['id']})
            if(capsule is None):
                # save capsule
                capsule = save_instance(Capsule, {
                    'capsule_id': caps['id'],
                    'reuse_count': caps['reuse_count'],
                    'water_landings': caps['water_landings'],
                    'land_landings': caps['land_landings'],
                    'last_update': caps['last_update'],
                    'serial': caps['serial'],
                    'status': caps['status'],
                    'type': caps['type']
                })
                
            # Find all launches of this capsule
            for launche_id in caps['launches']:
                # Get list of launches by cupsule
                end_point_launche = 'launches/'+launche_id
                launches = spacex.get_full_data_request(end_point_launche, params)

                launcheJson = launches.json()
                if launcheJson:

                    # Check if launches exist before save in Sqlite
                    launche = find(Launche, {'launche_id': launche_id})
                    if(launche is None):
                        # save launche
                        save_instance(Launche, {
                            'launche_id': launche_id,
                            'success': bool(launcheJson['success']),
                            'upcoming': bool(launcheJson['upcoming']),
                            'name': launcheJson['name'],
                            'flight_number': launcheJson['flight_number'],
                            'details': launcheJson['details'],
                            'static_fire_date_utc': launcheJson['static_fire_date_utc'],
                            'link_patch_image': launcheJson['links']['patch']['small'],
                            'link_patch_webcast': launcheJson['links']['webcast'],
                            'capsule': capsule
                        })
        
        messages.success(request, "Recuperation des capsules réalisée avec succès.")

    template_name = "capsule/index.html"
    context = {
        'page': {
            'title': 'Capsule',
            'name': 'Capsule'
        },
        'charge_active': True,
        'capsules': capsules
    }
    return render(request, template_name, context)


# ---------------- Get capsules list  -----------------
def capsule_list(request):

    template_name = "capsule/list.html"
    capsules = find_all(Capsule, {}) # get capsule liste
    context = {
        'page': {
            'title': 'Liste des capsule',
            'name': 'Capsule'
        },
        'list_active': True,
        'capsules': capsules
    }
    return render(request, template_name, context)


# ---------------- Get detail capsules  -----------------
def capsule_detail(request, id):

    # Find capsule or put error message and redirect
    capsule = find(Capsule, {'capsule_id': id})
    if capsule is None:
        messages.error(request, "Capsule introuvable.")
        return redirect ('capsule:capsule_list') 

    template_name = "capsule/detail.html"
    context = {
        'page': {
            'title': 'Detail capsule',
            'name': 'Detail capsule'
        },
        'list_active': True,
        'capsule': capsule
    }
    return render(request, template_name, context)



