from django.db import models
from utils.base_model import BaseModel
# Create your models here.


# https://github.com/r-spacex/SpaceX-API/blob/master/docs/capsules/v4/schema.md
class Capsule(BaseModel):

    STATUS_CHOICES = (
        ('unknown', 'Unknown'),
        ('active', 'Active'),
        ('retired', 'Retired'),
        ('destroyed', 'Destroyed'),
    )
    TYPE_CHOICES = (
        ('Dragon 1.0', 'Dragon 1.0'),
        ('Dragon 1.1', 'Dragon 1.1'),
        ('Dragon 2.0', 'Dragon 2.0'),
    )
    capsule_id = models.CharField(max_length=32, unique=True)
    reuse_count = models.PositiveIntegerField(default=0)
    water_landings = models.PositiveIntegerField(default=0)
    land_landings = models.PositiveIntegerField(default=0)
    last_update = models.CharField(max_length=255, blank=True, null=True)
    serial = models.CharField(max_length=8, unique=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=16, default='unknown')
    type = models.CharField(choices=TYPE_CHOICES, max_length=16, default='Dragon 1.0')

    def __str__(self):
        return self.capsule_id

    #Liste launches by capsule
    def launches(self):
        queryset = self.launche_set.all()
        return queryset if queryset else None


# https://github.com/r-spacex/SpaceX-API/blob/master/docs/launches/v4/schema.md
class Launche(BaseModel):
    launche_id = models.CharField(max_length=32, unique=True)
    success = models.BooleanField()
    upcoming = models.BooleanField(default=False)
    name = models.CharField(max_length=255)
    flight_number = models.PositiveSmallIntegerField(default=1)
    details = models.TextField(blank=True, null=True)
    static_fire_date_utc = models.CharField(max_length=255, blank=True, null=True)
    link_patch_image = models.URLField()
    link_patch_webcast = models.URLField()
    capsule = models.ForeignKey(Capsule, on_delete=models.CASCADE)

    def __str__(self):
        return self.launche_id


