from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
class BaseModel(models.Model):
    created_at = models.DateTimeField(_('created_at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'), auto_now=True)
    exclude_fields = ['_id', 'created_at', 'updated_at', ]

    objects = models.Manager()

    @classmethod
    def properties_list(cls):
        return [f.name for f in cls._meta.fields]

    @classmethod
    def filtered_list(cls):
        return [f.name for f in cls._meta.fields if
                f.name not in cls.exclude_fields and cls.get_fk_model(f) is None]

    @classmethod
    def get_fk_model(cls, field):
        """Returns None if not foreignkey, otherswise the relevant model"""
        if isinstance(field, models.ForeignKey):
            return field.related_model
        else:
            return None

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        abstract = True
