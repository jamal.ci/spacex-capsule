# -----------  Query -------------------
def find_all(models, data=dict()):
    return models.objects.filter(**data)


def find(models, data=dict()):
    try:
        return models.objects.get(**data)
    except models.DoesNotExist:
        return None


def save_instance(models, data=dict()):
    return models.objects.create(**data)

# -----------  API SpaceX -------------------
