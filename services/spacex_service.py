import requests


class SpaceXService():
    base_url = 'https://api.spacexdata.com/v4/'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    def __init__(self, headers = None):
        if headers is dict:
            self.headers = headers
    
    def get_full_url(self, end_point):
        return "{}{}".format(self.base_url, end_point)
    
    def set_headers(self, **headers):
        self.headers = headers
    
    
    #Getting full data
    def get_full_data_request(self, end_point, params={}):
        return requests.get(self.get_full_url(end_point), params=params)


